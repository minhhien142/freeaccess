
const MY_DOMAIN = "http://accessfree.tk";
function getCurrentTabUrl(callback) {
    var queryInfo = {
      active: true,
      currentWindow: true
    };
    chrome.tabs.query(queryInfo, (tabs) => {
      var tab = tabs[0];
      var url = tab.url;
      console.assert(typeof url == 'string', 'tab.url should be a string');
  
      callback(url);
    });
  }

chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
    if(changeInfo.status == "complete" ){
        getCurrentTabUrl((url) => {
            // chrome.tabs.executeScript({
            //     code: 'console.log("cccc'+url+'")'
            // })
            if(url.search(MY_DOMAIN) != -1){
                $.post(MY_DOMAIN + "/getdata",
                    {
                        id: url.split("/access/")[1]
                    },
                    function(data,status){
                        chrome.tabs.executeScript({
                            code: 'console.log('+data.cookies+')'
                        })
                        cookies = JSON.parse(data.cookies);
                        cookies.forEach(function(item) {
                            chrome.cookies.remove({
                                url: data.url,
                                name: item.name,
                                storeId: item.storeId
                            })
                            newcookie = {
                                url: data.url,
                                name: item.name,
                                value: item.value,
                                path: item.path,
                                secure: item.secure,
                                httpOnly: item.httpOnly,
                                expirationDate: item.expirationDate,
                                storeId: item.storeId
                             }
                             if(!item.hostOnly){
                                 newcookie.domain = item.domain
                             }
                            chrome.cookies.set(newcookie);
                        })

                        chrome.tabs.executeScript({
                            code: 'window.location.replace("'+ data.url+'")'
                        })
              
                });
            }
        })
        
    }
})